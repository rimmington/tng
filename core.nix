{ callPackage, lib }:

let
  # NB: terranix imports <nixpkgs> directly
  terranix = import ./terranix/core;

  genInputAttrs = concatMapAttrs (_: {details, ...}:
    lib.mapAttrsToList
      (_: a: lib.nameValuePair a.varName a)
      details
  );

  # All var attrs have { type, varName }

  outputAttr = mod: attr: def:
    if !(builtins.isAttrs def)
      then builtins.throw "${mod}.output.${attr} must be an attrset"
    else if !(def ? value)
      then builtins.throw "${mod}.output.${attr} must have a value attribute"
    else rec {
      inherit (def) value;
      type     = def.type or "string";
      mock     = def.mock or "MOCK-${mod}-${attr}";
      varName  = "dependency_${mod}__${attr}";
      depRef   = "\${dependency.${mod}.outputs.${attr}}";
    };

  varFileAttr = fn: attr: def:
    if !(builtins.isAttrs def)
      then builtins.throw "varFiles.${fn}.${attr} must be an attrset"
    else {
      type     = def.type or "string";
      example  = def.example or "";
      varName  = attr;
    };

  # Here are some unary functions
  #  * [bool] not (!)
  #  * [list] distinct
  #  * [list] toset
  #  * [number] abs
  #  * [number] ceil
  #  * [number] signum
  #  * [string] chomp
  #  * [string] lower
  #  * [string] trimspace
  #  * [string] upper
  #  * [string] base64decode
  #  * [string] base64encode
  #  * [string] urlencode

  # A variable expression is passed to `config`
  mkVarExpr =
    let
      __toString = a: "\${${a.bare}}";
      mkExpr = { bare, type, ... }: {
        inherit bare type __toString;

        base64decode = mkExpr { type = "string"; bare = "base64decode(${bare})"; };
        base64encode = mkExpr { type = "string"; bare = "base64encode(${bare})"; };
        not          = mkExpr { type = "bool";   bare = "!${bare}"; };
        urlencode    = mkExpr { type = "string"; bare = "urlencode(${bare})"; };
      };
    in { varName, type, ... }: mkExpr {
      inherit type;
      bare = "var.${varName}";
    };

  guardedMapAttrs = name: f: attrs:
    if builtins.isAttrs attrs
      then builtins.mapAttrs f attrs
      else builtins.throw "${name} must be an attrset";

  # (name: value: [nameValuePair]): {} => {}
  concatMapAttrs = f: attrs: builtins.listToAttrs
    (builtins.concatMap (n: f n attrs.${n}) (builtins.attrNames attrs));
in expr:
  let
    stages = lib.filterAttrs (n: _: n != "varFiles") expr;
    modOutputs = builtins.mapAttrs
      (n: v: guardedMapAttrs "${n}.output" (outputAttr n) (v.output or {}))
      stages;
    varFileOutputs = builtins.mapAttrs
      (n: guardedMapAttrs "varFiles.${n}" (varFileAttr n))
      (expr.varFiles or {});
    # => { source = "stage" | "varFile"; details = {...}; }
    resolve = dant: n:
      if modOutputs ? ${n}
        then if varFileOutputs ? ${n}
          then builtins.throw "${dant} depends on ${n} but both a module and varFile exist with that name"
          else { source = "stage"; details = modOutputs.${n}; }
        else if varFileOutputs ? ${n}
          then { source = "varFile"; details = varFileOutputs.${n}; }
          else builtins.throw "${dant} depends on non-existent module ${n}";
    gen = name: mod:
      let
        configBits =
          if !(mod ? config)
            then builtins.throw "${name} module missing config"
          else if builtins.isAttrs mod.config
            then {
              depNames = [];
              fun = _: mod.config;
            }
          else if builtins.isFunction mod.config && builtins.functionArgs mod.config != {}
            then {
              depNames = builtins.attrNames (builtins.functionArgs mod.config);
              fun = mod.config;
            }
          else builtins.throw "${name}.config must be attrset or function with formal arguments";
        resolved = lib.genAttrs configBits.depNames (resolve name);
        inputAttrs = genInputAttrs resolved;
        augVars = cfg: cfg // {
          variable = cfg.variable or {} //
            builtins.mapAttrs (_: a: { inherit (a) type; }) inputAttrs;
          output = cfg.output or {} //
            builtins.mapAttrs (_: a: { inherit (a) value; }) modOutputs.${name};
        };
        resolvedStages = lib.filterAttrs (_: v: v.source == "stage") resolved;
      in {
        tf = (terranix {
          terranix_config = augVars (configBits.fun
            (builtins.mapAttrs
              (_: {details, ...}: builtins.mapAttrs (_: mkVarExpr) details)
              resolved));
        }).config;
        tg = lib.recursiveUpdate (mod.terragrunt or {})
          (lib.optionalAttrs (resolvedStages != {}) {
            inputs = concatMapAttrs
              (n: a: if a ? depRef then [(lib.nameValuePair n a.depRef)] else [])
              inputAttrs;

            dependency = builtins.mapAttrs
              (n: {details, ...}:{
                config_path = "../${n}";
                mock_outputs = builtins.mapAttrs (_: a: a.mock) details;
              })
              resolvedStages;
          } // {
            terraform.extra_arguments.varFiles = {
              commands = [ "apply" "destroy" "plan" "refresh" ];
              arguments = builtins.concatLists (lib.mapAttrsToList
                (n: {source, ...}:
                  if source == "varFile"
                    then [ "-var-file" "../${n}.tfvars.json" ]
                    else []
                )
                resolved);
            };
          }
          );
      };
  in builtins.mapAttrs gen stages // {
    varFiles = builtins.mapAttrs (fn: builtins.mapAttrs (_: a: a.example)) varFileOutputs;
  }
