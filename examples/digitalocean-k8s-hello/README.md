# digitalocean-k8s-hello

A simple hello world Nginx container running on DigitalOcean-managed Kubernetes behind Cloudflare. Kubernetes network policies are configured so the service is only (externally) accessible via the DO Load Balancer. Nginx is configured with a Cloudflare-issued certificate for end-to-end ("Full") TLS. Terraform state is stored in Terraform Cloud.

## Prerequisites

* A [Terraform Cloud](https://app.terraform.io) account (free tier is sufficient)
* A domain name managed by Cloudflare
* A DigitalOcean account ([100 USD / 60 days referral link](https://m.do.co/c/838d0c9a3f18))

## Cost

20 USD + tax per month pro rata

## Deploying

1. `nix-shell`
2. Configure `topDomain` and `terraformOrg` in `config.nix`.
3. Run `tng -o output; cd output`.
4. Copy `secrets.example.tfvars.json` to `secrets.tfvars.json` and fill in the secret values.
5. `terragrunt apply-all`
