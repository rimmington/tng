let
  # Name of a domain controlled by Cloudflare
  topDomain = ;
  # app.terraform.io organisation name use to store Terraform state
  terraformOrg = ;

  # Common bindings
  app = "tng-example";
  subdomain = "tng-example";
  fullDomain = "${subdomain}.${topDomain}";

  secretPrivateKey = "private-key";
  secretPublicCert = "public-cert";

  # User must run `terraform login` to set up token
  backend = suffix: {
    remote = {
      hostname = "app.terraform.io";
      organization = terraformOrg;
      workspaces.name = "tng-example-${suffix}";
    };
  };
in {
  varFiles.secrets = {
    # API key used for Origin CA certificates, get from account settings
    cloudflareOriginCaKey = {};
    # Cloudflare token needs permission to read zone + edit DNS
    cloudflareRecordToken = {};
    # DigitalOcean personal access token from API tab
    doToken = {};
  };

  cluster.config = { secrets }: {
    terraform.backend = backend "cluster";

    provider.digitalocean.token = secrets.doToken;

    resource.digitalocean_kubernetes_cluster.tng_example = {
      name = "tng-example";
      region = "sgp1";
      # https://www.digitalocean.com/docs/kubernetes/changelog/
      version = "1.16.6-do.2";

      # 10 USD + tax per month
      node_pool = {
        name = "tng-example-pool";
        # https://slugs.do-api.dev/
        # https://www.digitalocean.com/docs/kubernetes/#allocatable-memory
        size = "s-1vcpu-2gb";
        node_count = 1;
      };
    };
  };

  cluster.output = {
    host.value = "\${digitalocean_kubernetes_cluster.tng_example.kube_config.0.host}";
    token.value = "\${digitalocean_kubernetes_cluster.tng_example.kube_config.0.token}";
    caCertificate.value = "\${digitalocean_kubernetes_cluster.tng_example.kube_config.0.cluster_ca_certificate}";
    exemplarNodeDropletName.value = "\${digitalocean_kubernetes_cluster.tng_example.node_pool.0.nodes.0.name}";
  };

  resources.config = { secrets, cluster }: {
    terraform.backend = backend "resources";

    provider = {
      digitalocean.token = secrets.doToken;

      cloudflare = {
        api_token = secrets.cloudflareRecordToken;
        api_user_service_key = secrets.cloudflareOriginCaKey;
      };

      kubernetes = {
        load_config_file = false;
        host = cluster.host;
        token = cluster.token;
        cluster_ca_certificate = cluster.caCertificate.base64decode;
      };
    };

    data = {
      digitalocean_droplet.exemplar_node.name = cluster.exemplarNodeDropletName;
      cloudflare_zones.domain.filter.name = topDomain;
    };

    resource = rec {
      #
      # Cloudflare-issued origin cert
      #
      tls_private_key.cf_origin_cert = {
        algorithm = "ECDSA";
        ecdsa_curve = "P256";
      };

      tls_cert_request.cf_origin_cert = {
        key_algorithm = "\${tls_private_key.cf_origin_cert.algorithm}";
        private_key_pem = "\${tls_private_key.cf_origin_cert.private_key_pem}";
        subject.common_name = fullDomain;
      };

      cloudflare_origin_ca_certificate.cf_origin_cert = {
        csr = "\${tls_cert_request.cf_origin_cert.cert_request_pem}";
        hostnames = [ fullDomain ];
        request_type = "origin-ecc";
        requested_validity = 7;
      };

      kubernetes_secret.cf_origin_cert = {
        metadata.name = "cf-origin-cert";
        data = {
          "${secretPublicCert}" = "\${cloudflare_origin_ca_certificate.cf_origin_cert.certificate}";
          "${secretPrivateKey}" = "\${tls_private_key.cf_origin_cert.private_key_pem}";
        };
      };

      #
      # Kubernetes network rules
      #
      kubernetes_network_policy = {
        default_deny = {
          metadata.name = "default-deny";
          spec = {
            policy_types = [ "Ingress" ];
            pod_selector.match_expressions = []; # all pods
            # no rules = no inbound traffic
          };
        };

        # Useful command for debugging, replace the pod name (cilium-) as per your cluster:
        #   kubectl --kubeconfig=tng-example-kubeconfig.yaml -n kube-system exec -ti cilium-fpd7s -- cilium monitor --type drop
        allow_nginx_svc =
          let
            svc = kubernetes_service.nginx_svc.spec;
          in {
            metadata.name = "allow-nginx-svc";
            spec = {
              policy_types = [ "Ingress" ];
              pod_selector.match_labels = svc.selector;
              # Only permit access via droplet internal network
              # Cannot also specify ports because of https://github.com/cilium/cilium/issues/4129
              ingress.from.ip_block.cidr = "\${data.digitalocean_droplet.exemplar_node.ipv4_address_private}/20";
            };
          };
      };

      #
      # Nginx configuration + pod
      #
      kubernetes_config_map.nginx_config = {
        metadata.name = "nginx-config";
        data."nginx.conf" = ''
          error_log stderr;

          user nginx;

          events {}

          http {
            client_max_body_size 100k;
            server_tokens off; # Show nginx version in headers and error pages.

            error_log stderr;
            access_log /dev/stdout;

            # generated 2020-03-22, Mozilla Guideline v5.4, nginx 1.17.7, OpenSSL 1.1.1d, modern configuration, no HSTS, no OCSP
            # https://ssl-config.mozilla.org/#server=nginx&version=1.17.7&config=modern&openssl=1.1.1d&hsts=false&ocsp=false&guideline=5.4
            ssl_session_timeout 1d;
            ssl_session_cache shared:MozSSL:10m;  # about 40000 sessions
            ssl_session_tickets off;
            # modern configuration
            ssl_protocols TLSv1.3;
            ssl_prefer_server_ciphers off;

            server {
              listen 443 ssl http2;

              ssl_certificate /etc/ssl/certs/certificate.pem;
              ssl_certificate_key /etc/ssl/private/private_key.pem;

              location / {
                add_header Content-Type text/plain;
                return 200 'Hello world!';
              }
            }
          }
        '';
      };

      kubernetes_pod.nginx =
        let nginxName = "nginx-conf";
        in {
          metadata = {
            name = "nginx";
            labels = { inherit app; };
          };
          spec.container = {
            name = "nginx";
            image = "nginx:1.17.9-alpine";
            volume_mount = [
              {
                name = secretPublicCert;
                mount_path = "/etc/ssl/certs";
                read_only = true;
              }
              {
                name = secretPrivateKey;
                mount_path = "/etc/ssl/private";
                read_only = true;
              }
              {
                name = nginxName;
                mount_path = "/etc/nginx/nginx.conf";
                sub_path = "nginx.conf"; # mount file instead of directory
                read_only = true;
              }
            ];
          };
          spec.volume =
            let
              certSecret = { key, path }: {
                name = key;
                secret = {
                  secret_name = kubernetes_secret.cf_origin_cert.metadata.name;
                  optional = false;
                  items = [ { inherit key path; mode = "0600"; } ];
                };
              };
            in [
              (certSecret { key = secretPublicCert; path = "certificate.pem"; })
              (certSecret { key = secretPrivateKey; path = "private_key.pem"; })
              {
                name = nginxName;
                config_map.name = kubernetes_config_map.nginx_config.metadata.name;
              }
            ];
        };

      #
      # Load balancer
      #
      # 10 USD + tax per month
      kubernetes_service.nginx_svc = {
        metadata = {
          name = "nginx-svc";
          annotations = {
            "service.beta.kubernetes.io/do-loadbalancer-name" = "nginx-svc-lb";
            # Need this for ignore_changes below
            "kubernetes.digitalocean.com/load-balancer-id" = "";
          };
        };
        spec = {
          type = "LoadBalancer";
          # https://kubernetes.io/docs/tutorials/services/source-ip/#source-ip-for-services-with-type-loadbalancer
          external_traffic_policy = "Local";
          selector = { inherit app; };
          port = [
            { name = "https"; port = 443; target_port = 443; }
          ];
        };
        # Stop trying to remove this DO-managed annotation
        lifecycle.ignore_changes = [ ''metadata[0].annotations["kubernetes.digitalocean.com/load-balancer-id"]'' ];
      };

      #
      # DNS
      #
      cloudflare_record.subdomain = {
        zone_id = "\${data.cloudflare_zones.domain.zones.0.id}";
        name = subdomain;
        type = "A";
        value = "\${kubernetes_service.nginx_svc.load_balancer_ingress.0.ip}";
        proxied = true;
      };
    };
  };
}
