## 1.0.0 – 02020-04-08
#### Added
- tng executable
- Bundle patched terranix
- Stage/module interdependencies via Terragrunt
- varFiles support
- `not`, `base64decode`, `base64encode`, `urlencode` expression support
- GPLv3 LICENSE
- README
