{ nixpkgs ? import <nixpkgs> }:

let
  overlay = import ./shell-overlay.nix;
  pkgs = nixpkgs { overlays = [ overlay ]; };
  tng = pkgs.callPackage ./default.nix {};
in pkgs.mkShell {
  buildInputs = [ tng pkgs.terragrunt ];
}
