self: super:

let
  unstableSrc = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/991bbef68351b4aa228f5f763c8d2ded2aeeb84e.tar.gz";
    sha256 = "0wq4xqm0qliphc11sj08mp8586r039l6v1x1a2cpvb66sh36a54w";
  };
in {
  terraform-providers =
    self.callPackage "${unstableSrc}/pkgs/applications/networking/cluster/terraform-providers" {};
  terraform_0_12 =
    (self.callPackage "${unstableSrc}/pkgs/applications/networking/cluster/terraform" {}
    ).terraform_0_12;
  terragrunt =
    self.callPackage "${unstableSrc}/pkgs/applications/networking/cluster/terragrunt" {
      terraform.full = self.terraform.withPlugins (ps: with ps;
        [ cloudflare digitalocean kubernetes ps."null" terraform tls ]
      );
    };
}
