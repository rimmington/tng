# tng

DRY multi-stage Terraform deployments using NixOS tooling.

[terranix][] lets you use the Nix language and NixOS modules to create [Terraform][] configuration. [Terragrunt][] supports (amongst other things) inter-module dependencies, [`apply-all`][] and providing Terraform CLI arguments via configuration files. **tng** combines them.

See the `examples/` directory for an example configuration with deployment instructions.

[terranix]: https://terranix.org/
[Terraform]: https://www.terraform.io/
[Terragrunt]: https://terragrunt.gruntwork.io/
[`apply-all`]: https://terragrunt.gruntwork.io/docs/features/execute-terraform-commands-on-multiple-modules-at-once/

## Installation

Add the following to your project `shell.nix` or your `home.nix`:

```nix
callPackage (
  fetchFromGitLab {
    owner  = "rimmington";
    repo   = "tng";
    rev    = "v1.0.0";
    sha256 = "19fz3nnhn8sqcix5fpbhcz3gj04bddd6m6h2811bq1dgbnk12x8x";
  }
) {}
```

You will also want to install Terragrunt from Nixpkgs to plan and apply configurations.

## Usage

See `tng --help`.

## Configuration

A tng configuration is an attrset of _stages_ (Terraform modules), plus the optional special attribute `varFiles`:

```nix
{
  varFiles.myVarFile = { ... };
  stage1 = {
    config = { ... };
    output = { ... };
  };
  stage2.config = { stage1, myVarFile }: { ... };
}
```

### Stages

A stage _must_ be an attrset with attributes:

* `config`:
  * An attrset holding a Terranix configuration, or
  * A function with formal arguments, each argument being the name of a stage or [_varfile_](#varfiles), that returns a Terranix configuration.
* `output` (optional): an attrset of [_outputs_](#outputs).
* `terragrunt` (optional): an attrset of values to be merged into `terragrunt.hcl.json`.

If `config` is a function, each argument is made a dependency of the stage via Terragrunt. The function is passed attrsets of [variable references](#variable-references). Caveat: if a stage depends on two varfiles that provide the same variable, the resulting behaviour is undefined.

For further instruction on what `config` should return, see the [terranix][] documentation and the [Terraform JSON syntax][] documentation.

#### Outputs

[Terraform outputs][] should be defined in the stage `output` attribute rather than under `config`.

The name of each attribute of `output` is a [Terraform identifier][Terraform identifiers], and the value is an attrset with attributes:

* `value`: a string containing a Terraform expression, typically an interpolation expression like `"\${resource.type.name.attribute}"`.
* `type` (optional): a string containing the [Terraform type][] of the output. Defaults to `"string"`.
* `mock` (optional): a string containing the [mock value][] of the output. Defaults to `"MOCK-<stage>-<name>"`.

Unlike variables in varfiles, output names do not have to be unique across all stages that a particular stage depends on, as they are prefixed automatically.

#### Variable references

When a stage depends on other stages or varfiles via `config` function arguments, corresponding Terragrunt dependencies and Terraform input variables are automatically created. The `config` function is passed an attrset of _variable references_ for each dependency. A variable reference is an attrset with the special property that Nix `toString` or antiquotation syntax `${...}` will coerce the reference to [Terraform interpolation][] syntax. Any references that appear alone as a value of an attrset in the result of `config` are automatically coerced to interpolation syntax also, which is required in the [Terraform JSON syntax][]. This lets you treat variable references somewhat like Nix bindings:

```nix
myStage.config = { myVarFile }: {
  provider.digitalocean.token = myVarFile.digitaloceanToken;
  tls_private_key.mycert.ecdsa_curve = "P${myVarFile.certBitStrength}";
}
```

Variable references have an attribute `bare` containing the bare expression, ie. without Terraform interpolation brackets, which is useful when using a variable in a more complex Terraform expression. They also have a number of attributes that return "variable" references that apply a small selection of Terraform functions: `not` (`!`), [`base64decode`][], [`base64encode`][] and [`urlencode`][]. For example, `myVarFile.myVariable.urlencode.bare = "urlencode(var.myVariable)"`.

[`base64decode`]: https://www.terraform.io/docs/configuration/functions/base64decode.html
[`base64encode`]: https://www.terraform.io/docs/configuration/functions/base64encode.html
[`urlencode`]: https://www.terraform.io/docs/configuration/functions/urlencode.html

### Varfiles

The top-level `varFiles` attribute, if provided, must be an attrset. The name of each attribute is the name of the varfile. The value of each attribute is also an attrset, where the names are valid [Terraform identifiers][] and the values are _variable descriptions_. A variable description is an attrset with attributes:

* `type` (optional): a string containing the [Terraform type][] of the variable. Defaults to `"string"`.
* `example` (optional): a string containing a value for the `.example.tfvars.json` file. Defaults to `""`.

tng generates a `<name>.example.tfvars.json` for each attribute in `varFiles`. When a [stage](#stages) depends on a varfile, it is automatically provided via Terragrunt.

[Terraform identifiers]: https://www.terraform.io/docs/configuration/syntax.html#identifiers
[Terraform outputs]: https://www.terraform.io/docs/configuration/outputs.html
[Terraform type]: https://www.terraform.io/docs/configuration/types.html
[Terraform interpolation]: https://www.terraform.io/docs/configuration/expressions.html#interpolation
[Terraform JSON syntax]: https://www.terraform.io/docs/configuration/syntax-json.html
[mock value]: https://terragrunt.gruntwork.io/docs/features/execute-terraform-commands-on-multiple-modules-at-once/#unapplied-dependency-and-mock-outputs
