{ bash, fetchFromGitHub, jq, runCommand, stdenvNoCC, writeScriptBin, writeText }:

let
  usage = writeText "usage" ''
    Usage: tng [--show-trace] -o output [path]
           tng --help
      -h | --help    print help
      --show-trace   show trace information if there is an error
      -o | --output  path to output directory
      path           path to config.nix
  '';
  core =
    let terranix = stdenvNoCC.mkDerivation {
      name = "terranix";

      src = fetchFromGitHub { # master at 02020-03-07
        owner  = "mrVanDalo";
        repo   = "terranix";
        rev    = "dfbf4d1fae08da8052ff880c5d02b2eb5857d54c";
        sha256 = "1qilbvldlq7ybxa3yx99hb8vbmj0sk5x9qqxa4f1czpzj2mja0fn";
      };

      phases = [ "unpackPhase" "patchPhase" "installPhase" ];

      patches = [
        ./terranix-toString.patch
      ];

      installPhase = ''
        cp -r . $out
      '';
    };
    in runCommand "tng-core" {} ''
      mkdir $out
      cd $out
      cp ${./core.nix} default.nix
      ln -s ${terranix} terranix
    '';
in writeScriptBin "tng" ''
  #!${bash}/bin/bash
  set -euo pipefail

  PATH=${jq}/bin:$PATH

  TRACE=""
  FILE="./config.nix"
  OUTPUT=""

  while [[ $# -gt 0 ]]
  do
    case $1 in
      --help | -h)
        cat ${usage}
        exit 0
        ;;
      --show-trace | --trace)
        TRACE="--show-trace"
        shift
        ;;
      --output | -o)
        shift
        OUTPUT=$1
        shift
        ;;
      *)
        FILE=$1
        shift
        break
        ;;
    esac
  done

  if [ ! -f $FILE ]; then
    echo "$FILE does not exist"
    exit 1
  fi

  if [ -z "$OUTPUT" ]; then
    echo "-o must be specified"
    exit 1
  fi

  expr="with (import <nixpkgs> {}); writeText \"tng-out\" (builtins.toJSON (callPackage ${core} {} (import $FILE)))"
  json=$(nix-build --no-out-link $TRACE -E "($expr)")

  if [ -d $OUTPUT ]; then
    rm -f $OUTPUT/**/main.tf.json $OUTPUT/**/terragrunt.hcl.json $OUTPUT/**/.here.tf
  else
    mkdir $OUTPUT
  fi

  for k in $(cat "$json" | jq --raw-output 'del(.varFiles) | keys | join("\n")'); do
    mkdir -p "$OUTPUT/$k"
    cat "$json" | jq ".[\"$k\"].tf" > "$OUTPUT/$k/main.tf.json"
    cat "$json" | jq ".[\"$k\"].tg" > "$OUTPUT/$k/terragrunt.hcl.json"
    touch "$OUTPUT/$k/.here.tf"
  done

  for k in $(cat "$json" | jq --raw-output '.varFiles | keys | join("\n")'); do
    cat "$json" | jq ".varFiles | .[\"$k\"]" > "$OUTPUT/$k.example.tfvars.json"
  done
''
